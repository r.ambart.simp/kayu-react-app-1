import ProductSources from "../../store/constants/ProductSources";

type StoreProduct = {
    product: FullProduct,
    date: Date,
    source: ProductSources
}