import { useNavigation } from "@react-navigation/core";
import {
  BarCodeEvent,
  BarCodeScannedCallback,
  BarCodeScanner,
} from "expo-barcode-scanner";
import React, { useEffect, useState } from "react";
import { Button, StyleSheet, Text, View } from "react-native";
import Layout from "../components/shared/Layout";
import { Routes } from "../navigation/constants";
import { getInfosByCodeProduit } from "../api/OpenFoodFactsApi";
import ProductSources from "../store/constants/ProductSources";
import { addProductToStorage } from "../store/HistoryStore";

type ScanProps = {};

export default function Scan({}: ScanProps) {
  const [hasPermission, setHasPermission] = useState<boolean | null>(null);
  const [scanned, setScanned] = useState(false);
  const { navigate } = useNavigation();

  useEffect(() => {
    (async () => {
      const { status } = await BarCodeScanner.requestPermissionsAsync();
      setHasPermission(status === "granted");
    })();
  }, []);

  const handleBarCodeScanned: BarCodeScannedCallback = ({
    type,
    data,
  }: BarCodeEvent) => {
    try {
      setScanned(true);
      getInfosByCodeProduit(data).then((result) => {
        const product = result.data;

        if (!product || result.status === 0)
          throw new Error("Impossible de retrouver les données du produit.");
        else {
          addProductToStorage(product, ProductSources.SCAN).then((res) => {
            navigate(Routes.PRODUCT, { product });
          });
        }
      });
    } catch (error) {
      console.error(error);
    }
  };

  if (hasPermission === null) {
    return <Text>Requesting for camera permission</Text>;
  }
  if (hasPermission === false) {
    return <Text>No access to camera</Text>;
  }

  return (
    <Layout title={"Scan"}>
      <View style={styles.container}>
        <BarCodeScanner
          onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
          style={StyleSheet.absoluteFillObject}
        />
        {scanned && (
          <Button
            title={"Tap to Scan Again"}
            onPress={() => setScanned(false)}
          />
        )}
      </View>
    </Layout>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#fff",
  },
  barCodeView: {
    width: "100%",
    height: "50%",
    marginBottom: 40,
  },
});
