import AsyncStorage from "@react-native-async-storage/async-storage";
import { StoreProduct } from "../types/store/StoreProduct";
import ProductSources from "./constants/ProductSources";

const HISTORY_STORE_KEY = "HISTORY_STORE_KEY";

const addProductToStorage = async (
  product: FullProduct,
  source: ProductSources
) => {
  let products = await getProductsFromStorage();
  AsyncStorage.setItem(
    HISTORY_STORE_KEY,
    JSON.stringify([...products, buildStoreProduct(product, source)])
  );
};

const getProductsFromStorage = (): Promise<StoreProduct[]> => {
  return AsyncStorage.getItem(HISTORY_STORE_KEY).then(
    (res) => JSON.parse(res || "[]") as StoreProduct[]
  );
};

const buildStoreProduct = (
  product: FullProduct,
  source: ProductSources
): StoreProduct => {
  return { product: product, date: new Date(), source };
};

export { addProductToStorage, getProductsFromStorage };
