import React from "react";
import { View } from "react-native";
import { FlatList } from "react-native-gesture-handler";
import Ingredient from "./Ingredient";

type IngredientListProps = {
  ingredients: Ingredient[];
};
export default function IngredientList({ ingredients }: IngredientListProps) {
  const sortByNumber = (a: Ingredient, b: Ingredient) =>
    a.percent_estimate - b.percent_estimate > 0 ? -1 : 1;

  return (
    <View>
      {ingredients.length > 0 && (
        <FlatList
          data={ingredients.sort(sortByNumber)}
          renderItem={({ item }) => <Ingredient ingredient={item} />}
        />
      )}
    </View>
  );
}
