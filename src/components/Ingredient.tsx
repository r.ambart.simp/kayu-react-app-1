import React from "react";
import { Text, View } from "react-native";

type IngredientProps = {
  ingredient: Ingredient;
};
export default function Ingredient({ ingredient }: IngredientProps) {
  return (
    <View>
        <Text>{Math.floor(ingredient.percent_estimate * 100 / 100)}% - {ingredient.text}</Text>
    </View>
  );
}
