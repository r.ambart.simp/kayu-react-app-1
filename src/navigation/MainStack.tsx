import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import React from "react";
import { RootStackParamList } from ".";
import { Routes } from "./constants";
import { FontAwesome } from "@expo/vector-icons";
import History from "../views/History";
import Home from "../views/Home";
import ScanStack from "./ScanStack"

type tabBarIconProps = {
  focused: boolean;
  color: string;
  size: number;
}

const { Navigator: Stack, Screen } =
  createBottomTabNavigator<RootStackParamList>();

const bottomTabIcon = (str: string) => 
  ({ focused }: tabBarIconProps ) => (<FontAwesome name={str as any} size={24} color={focused ? "blue" : "black"} />
)

const MainStack = () => {
  return (
    <Stack tabBarOptions={{ activeTintColor: "blue" }}>
      <Screen
        options={{
          tabBarIcon: bottomTabIcon("history"),
          tabBarLabel: "Historique",
        }}
        name={Routes.HISTORY}
        component={History}
      />
      <Screen
        options={{
          tabBarIcon: bottomTabIcon("user"),
          tabBarLabel: "Home",
        }}
        name={Routes.HOME}
        component={Home}
      />
      <Screen
        options={{
          tabBarIcon: bottomTabIcon("camera"),
          tabBarLabel: "Scanner",
        }}
        name={Routes.SCAN}
        component={ScanStack}
      />
    </Stack>
  );
};

export default MainStack;
