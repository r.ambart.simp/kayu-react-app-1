import React from "react";
import { RootStackParamList } from ".";
import { Routes } from "./constants";
import { createStackNavigator } from "@react-navigation/stack";
import Product from "../views/Product";
import Scan from "../views/Scan";

const { Navigator: Stack, Screen } = createStackNavigator<RootStackParamList>();

const ScanStack = () => {
  return (
    <Stack initialRouteName={Routes.SCAN}>
      <Screen name={Routes.SCAN} component={Scan} />
      <Screen name={Routes.PRODUCT} component={Product} />
    </Stack>
  );
};

export default ScanStack;
