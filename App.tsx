import React from "react";
import { SafeAreaView, StyleSheet } from "react-native";
import "react-native-gesture-handler";
import { StatusBar } from "expo-status-bar";
import Main from "./src/Main";
import { SafeAreaProvider } from "react-native-safe-area-context";

export default function App() {
  return (
    <SafeAreaProvider>
      <Main />
      <StatusBar />
    </SafeAreaProvider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
});
